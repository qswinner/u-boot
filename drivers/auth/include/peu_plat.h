#ifndef _PEU_PLAT_H_
#define _PEU_PLAT_H_

#include <phytium_sip.h>



void pci_init(void);
uint32_t is_peu_link(uint32_t peu);
uint32_t is_peu_ep(uint32_t peu);
void pci_retraining(void);
void pci_flagclean(void);



#ifndef	COMPILE_PCI
#ifdef	CONFIG_PCI
	#error "if defined CONFIG_PCI must include peu.mk in platform.mk to compile peu project "
#endif
#endif

#endif
