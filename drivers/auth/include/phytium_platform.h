/*
 * Copyright (c) 2015-2018, ARM Limited and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */
#ifndef __PHYTIUM_PLATFORM_H__
#define __PHYTIUM_PLATFORM_H__
#include <soc.h>
#define CPU_MAILBOX_ADDR			(BL31_LIMIT + 0x1000)

#define SRAM_MISC_END				(SRAM_MISC_START + 0x100)

#define CPU_SCPI_LOCK				(SRAM_MISC_START + 0x40)
#define CPU_S3_SETUP_ADDR           (SRAM_MISC_START + 0x38)
#define CPU_S3_FLAG_ADDR            (SRAM_MISC_START + 0x30)
#define CPU_ID_ADDR                 (SRAM_MISC_START + 0x28) //get cpu id from this addr
#define CPU_RELEASE_ADDR            (SRAM_MISC_START + 0x20)

#define SRAM_MISC_START           	(SRAM_BASE + 0xe000)



#define PM_HEAD_OFFSET          (0x0)
#define PM_COMMON_OFFSET        (0x100)
#define PM_PEU_OFFSET           (0x200)
#define PM_OS_INFO_OFFSET       (0x300)
#define PM_DEV_INTERFACE_OFFSET (0x400)
#define PM_MCU_OFFSET           (0x500)

#endif
