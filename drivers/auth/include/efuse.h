#ifndef __EFUSE__H__
#define __EFUSE__H__

enum LCS_SATAE {
	CMRAM = 0x1,
	DMRAM = 0x2,
	SE = 0x4,
	DM = 0x8,
	CM = 0x10,
};

enum KEY_CHANLE {
	HUK_KEY_1 = 0,
	HUK_KEY_2,
	RTL_KEY,
	KPICV_KEY,
	KCEICV_KEY,
	KCP_KEY,
	KCE_KEY
};

void scto_test(int chanle, uint8_t *iv, uint8_t * std_plain, uint8_t * std_cipher);
void efuse_test(void) ;
void lcs_test(void);
void efuse_write_read_test(unsigned long base, unsigned int val, int length);
int efuse_write_32(unsigned long base, unsigned int data);
void change_lcs(int mode);
int get_lcs(void);
void huk1_sm4_test(void);
void huk2_sm4_test(void);
void kpicv_sm4_test(void);
void kceicv_sm4_test(void);
void kcp_sm4_test(void);
void kce_sm4_test(void);
void kpicv_tmp_sm4_test(void);
void kceicv_tmp_sm4_test(void);
void kcp_tmp_sm4_test(void);
void kce_tmp_sm4_test(void);
void rtl_sm4_test(void);

#endif
