#ifndef _TRNG_H_
#define _TRNG_H_

#ifdef __cplusplus
extern "C" {
#endif



#include "smx_common.h"




//TRNG register
#define TRNG_CR				(*((volatile uint32_t *)(TRNG_BASE_ADDR)))
#define TRNG_RTCR			(*((volatile uint32_t *)(TRNG_BASE_ADDR+0x04)))
#define TRNG_SR				(*((volatile uint32_t *)(TRNG_BASE_ADDR+0x08)))
#define TRNG_DR				(*((volatile uint32_t *)(TRNG_BASE_ADDR+0x0C)))
#define TRNG_FIFO_CR		(*((volatile uint32_t *)(TRNG_BASE_ADDR+0x20)))
#define TRNG_FIFO_SR		(*((volatile uint32_t *)(TRNG_BASE_ADDR+0x24)))
#define TRNG_HT_SR			(*((volatile uint32_t *)(TRNG_BASE_ADDR+0x70)))
#define TRNG_RO_CR			(*((volatile uint32_t *)(TRNG_BASE_ADDR+0x80)))
#define TRNG_RO_CR2			(*((volatile uint32_t *)(TRNG_BASE_ADDR+0x84)))
#define TRNG_RO_CR3			(*((volatile uint32_t *)(TRNG_BASE_ADDR+0x88)))




#define DEFAULT_FIFO_DEPTH  (0x7F)


#define TRNG_RO_FREQ_4      (0)
#define TRNG_RO_FREQ_8      (1)
#define TRNG_RO_FREQ_16     (2)
#define TRNG_RO_FREQ_32     (3)     //default



//TRNG return code
enum TRNG_RET_CODE
{
	TRNG_SUCCESS = 0,
	TRNG_BUFFER_NULL,
	TRNG_HT_FAILURE,
	TRNG_READ_DATA_NULL,
};




//API

uint8_t get_rand(uint8_t *a, uint32_t byteLen);



#ifdef __cplusplus
}
#endif

#endif
