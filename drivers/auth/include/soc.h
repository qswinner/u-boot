/*
 * (C) Copyright 2013
 * Phytium Technology Co.,Ltd <www.phytium.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef _SOC_H
#define _SOC_H

/* Generic Timer Frequency */
#define COUNTER_FREQUENCY	48000000	/* 48MHz */

#define PROCESSOR_PANEL_COUNT   1
#define PROCESSOR_LMU_COUNT     2

#define FLASH_BASE      0x00000000

#define MIO_BASE		0x28000000	/* 1MB: Uart/i2c/wdt/gpio */
#define CRU_BASE		0x28100000	/* 1MB */
#define MISC_BASE 		0x28180000
#define GPIO_BASE 		0x28004000

#define TSGW_BASE		0x28203000	/* 4KB: Time Stamp Generator */
#define TSGR_BASE		0x28204000	/* 4KB: Time Stamp Generator */

#define SRAM_BASE		0x29800000	/* 128KB: SRAM Space */
#define SRAM_SIZE		0x20000		/* 128KB: SRAM Space */

#define DCM_BASE		0x28700000	/* 512KB: Directory Manager */
#define MCU_BASE		0x28200000	/* 8KB */
#define MCU_STRIDE		0x1000		/* 4KB per MCU, totally 2 MCUs */

#define PCIE_BASE		0x29000000	/* 8MB: PCIe and SMMU */
#define ICU_BASE		0x29a00000	/* 4MB */
#define QSPI_BASE       0x28014000

#define QSPI_FLASH_CAPACITY_REG		(QSPI_BASE + 0X0)
#define QSPI_ADDR_READ_CFG_REG		(QSPI_BASE + 0X4)
#define QSPI_CMD_PORT_REG			(QSPI_BASE + 0X10)
#define QSPI_LD_PORT_REG			(QSPI_BASE + 0X1C)

/* CRU Registers */
#define CRU_SOFTRST_ALL		(CRU_BASE + 0x200)
#define CRU_FER_MASK		(CRU_BASE + 0x204)
#define CRU_RST_SOURCE		(CRU_BASE + 0x300)
#define RESET_PWRON			0x0
#define RESET_RESERVE		0x1
#define RESET_SOFTWARM		0x2
#define RESET_COREWARM		0x3
#define RESET_PSO			0x4
#define RESET_FATALERROR	0x5
#define RESET_SOFTLOCAL		0x6
#define RESET_WDT			0x7

/* #definitions of power management */
#define CLUST_PSOCFG_REG   (MISC_BASE + 0x0580)
#define CLUST_PSOSTAT_REG  (MISC_BASE + 0x0588)

/* Time Stamp Generator Registers */
#define TSGW_TSGEN0_CTLR	(TSGW_BASE)
#define TSGR_TSGEN0_READ	(TSGR_BASE)
#define TSGW_COUNTER_CTLR	(TSGW_TSGEN0_CTLR)	/* Counter Control */


/* GIC Address Space gic v3*/
#define GICD_BASE		(ICU_BASE)		/* 64KB Distributor */
#define GITS_BASE		(ICU_BASE + 0x20000)	/* 64KB ITS */
#define GICR_BASE		(ICU_BASE + 0x100000)	/* 8MB ReDistributor */
/* for gicv2 */
#define GICV2_GICD_BASE		(ICU_BASE + 0x1000)
#define GICV2_GICC_BASE		(ICU_BASE + 0x2000)

/* network on chip */
#define NOC_BASE                    0x3A000000
#define CLUSTER0_SNOOP_NODE_ID      0x1
#define CLUSTER1_SNOOP_NODE_ID      0x13
/* MN base */
#define MN_BASE                     (NOC_BASE + 0x0)
#define MN_DVM_DOMAIN_SET_OFFSET    0x210
#define MN_DVM_DOMAIN_CLR_OFFSET    0x220
/* HNF base */
#define HNF_BASE                    (NOC_BASE + 0x00200000)
#define HNF_COUNT                   0x8
#define HNF_STRIDE                  0x10000

#define HNF_PSTATE_REQ      (HNF_BASE + 0x10)
#define HNF_PSTATE_STAT     (HNF_BASE + 0x18)
#define HNF_PSTATE_OFF      0x0
#define HNF_PSTATE_SFONLY   0x1
#define HNF_PSTATE_HALF     0x2
#define HNF_PSTATE_FULL     0x3

#define HNF_SNOOP_DOMAIN_SET_OFFSET 0x210   /*set means : add node into snooping */
#define HNF_SNOOP_DOMAIN_CLR_OFFSET 0x220   /*clr means : remove node form snooping */

/* PCIe Config/IO/MEM Address Space  */
#define PCIE_CONFIG_BASE	0x40000000
#define PCIE_CONFIG_SIZE	0x10000000	/* 256MB */
#define PCIE_IO_BASE		0x50000000
#define PCIE_IO_SIZE		0x00F00000	/* 15MB */
#define PCIE_MEM32_BASE		0x58000000
#define PCIE_MEM32_SIZE		0x28000000	/* 640MB */
#define PCIE_MEM64_BASE		0x1000000000
#define PCIE_MEM64_SIZE		0x1000000000	/* 64GB */
#define PCIE_PHYS_TO_BUS(x)	(x & 0x7ffffffffff)

#define INTA		28
#define INTB 		29
#define INTC		30
#define INTD 		31

/*--------------------------------------------------------------*/
/*
 * Detailed Register Definitions
 */
/* MIO Address Space */
#define UART0_BASE		(MIO_BASE)			/* 9 pin 16550a */
#define UART1_BASE		(MIO_BASE + 0x1000)	/* 3 pin 16550a */
#define UART2_BASE		(MIO_BASE + 0x2000)
#define UART3_BASE		(MIO_BASE + 0x3000)
#define GPIO0_BASE		(MIO_BASE + 0x4000)
#define GPIO1_BASE		(MIO_BASE + 0x5000)
#define I2C0_BASE		(MIO_BASE + 0x6000)
#define I2C1_BASE		(MIO_BASE + 0x7000)
#define I2C2_BASE		(MIO_BASE + 0x8000)
#define I2C3_BASE		(MIO_BASE + 0x9000)
#define WDT0_BASE		(MIO_BASE + 0xA000)
#define WDT1_BASE		(MIO_BASE + 0xB000)

/*EFUSE*/
#define EFUSE_BASE				(0x2820A000)
#define EF_CHIP_ID				(EFUSE_BASE + 0X10)
#define EF_GMAC_ID				(EFUSE_BASE + 0x38)
#define MAP_STATE				(EFUSE_BASE + 0x404)
#define MAP_INT					(EFUSE_BASE + 0x408)
#define MAP_WRCLK				(EFUSE_BASE + 0x450)
#define WRUNLOCKED_KEY			(EFUSE_BASE + 0x494)

#define DFT_BASE  				(EFUSE_BASE + 0x7C)
#define HUK_BASE				(EFUSE_BASE + 0x80)
#define KPICV_BASE				(EFUSE_BASE + 0xA0)
#define KCEICV_BASE				(EFUSE_BASE + 0xB0)
#define ICVFLAG					(EFUSE_BASE + 0xC0)
#define	HBK_BASE				(EFUSE_BASE + 0xC4)
#define KCP_BASE				(EFUSE_BASE + 0xE4)
#define	KCE_BASE				(EFUSE_BASE + 0xF4)
#define OEMFLAG					(EFUSE_BASE + 0x104)
#define ROLLBACK_COUTNER        (EFUSE_BASE + 0x108)
#define USERREV_BASE			(EFUSE_BASE + 0x11C)
#define USERKEY_BASE			(EFUSE_BASE + 0x180)

#define KEYBUFF_DFT				(EFUSE_BASE + 0x27C)
#define KEYBUFF_HUK				(EFUSE_BASE + 0x280)
#define KEYBUFF_KPICV			(EFUSE_BASE + 0x2A0)	
#define KEYBUFF_KCEICV			(EFUSE_BASE + 0x2B0)
#define KEYBUFF_ICVFLAG			(EFUSE_BASE + 0x2C0)
#define KEYBUFF_HBK				(EFUSE_BASE + 0x2C4)
#define KEYBUFF_KCP				(EFUSE_BASE + 0x2E4)
#define	KEYBUFF_KCE				(EFUSE_BASE + 0x2F4)
#define KEYBUFF_OEMFLAG			(EFUSE_BASE + 0x304)
#define KEYBUFF_ROLLBACK_COUTNER (EFUSE_BASE + 0x308)
#define KEYBUFF_USERREV 		(EFUSE_BASE + 0x31C)
#define KEYBUFF_USERKEY 		(EFUSE_BASE + 0x380)


#endif /* _D2000_SOC_H */


