/*
 * =====================================================================================
 *
 *       Filename:  pll.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2019年6月28日
 *       Revision:  none
 *
 *         Author:  Wuxiangdi, wuxiangdi@phytium.com.cn
 *        Company:  Phytium
 *        License:  Dual BSD/GPL
 *
 * =====================================================================================
 */
#ifndef _D2000_PLL_H_
#define _D2000_PLL_H_

#include <soc.h>


#define REG_PLL_C0_CONF		(CRU_BASE + 0x0600)
#define REG_PLL_C1_CONF		(CRU_BASE + 0x0604)
#define REG_PLL_NOC_CONF	(CRU_BASE + 0x0608)
#define REG_PLL_LMU_CONF	(CRU_BASE + 0x060C)
#define REG_PLL_FIO_CONF	(CRU_BASE + 0x0610)
#define REG_PLL_GMU_CONF	(CRU_BASE + 0x0614)
#define REG_PLL_DMM_CONF	(CRU_BASE + 0x0618)
#define REG_PLL_SCTO_CONF	(CRU_BASE + 0x061C)

#define REG_PLL_C0_REAL		(CRU_BASE + 0x0700)
#define REG_PLL_C0_1_REAL	(CRU_BASE + 0x0704)
#define REG_PLL_C1_REAL		(CRU_BASE + 0x0708)
#define REG_PLL_C1_1_REAL	(CRU_BASE + 0x070C)
#define REG_PLL_NOC_REAL	(CRU_BASE + 0x0710)
#define REG_PLL_NOC_1_REAL	(CRU_BASE + 0x0714)

#define REG_PLL_LMU_REAL	(CRU_BASE + 0x0718)
#define REG_PLL_FIO_REAL	(CRU_BASE + 0x071C)
#define REG_PLL_GMU_REAL	(CRU_BASE + 0x0720)
#define REG_PLL_DMM_REAL	(CRU_BASE + 0x0724)
#define REG_PLL_SCTO_REAL	(CRU_BASE + 0x0728)

#define REG_DFS_STAT		(CRU_BASE + 0x0810)

#define PLL_3000M    0x1102007d
#define PLL_2950M    0x110701ae  //2948.57
#define PLL_2900M    0x110701a7
#define PLL_2850M    0x110801db
#define PLL_2800M    0x110300af
#define PLL_2750M    0x11070191  //2749.71
#define PLL_2700M    0x110400e1
#define PLL_2650M    0x11050114  //2649.6
#define PLL_2600M    0x11060145
#define PLL_2550M    0x11070174
#define PLL_2500M    0x110c0271

#define PLL_2450M    0x11010033  //2448.0
#define PLL_2400M    0x11010032
#define PLL_2350M    0x110d027c  //2348.31
#define PLL_2300M    0x110c023f
#define PLL_2250M    0x11080177
#define PLL_2200M    0x11060113
#define PLL_2150M    0x110500e0
#define PLL_2100M    0x110400af
#define PLL_2050M    0x1107012b
#define PLL_2000M    0x1103007d
#define PLL_1950M    0x11080145

#define PLL_1900M    0x110500c6
#define PLL_1850M    0x110d01f5  //1849.85
#define PLL_1800M    0x1102004b
#define PLL_1750M    0x110d01da  //1750.15
#define PLL_1700M    0x110700f8
#define PLL_1650M    0x11080113
#define PLL_1600M    0x11030064
#define PLL_1550M    0x12050143
#define PLL_1500M    0x1202007d
#define PLL_1450M    0x120701a7
#define PLL_1400M    0x120300af

#define PLL_1350M    0x120400e1
#define PLL_1300M    0x12060145
#define PLL_1250M    0x120801a1  //1251.0
#define PLL_1200M    0x12010032
#define PLL_1150M    0x1208017f  //1149.0
#define PLL_1100M    0x12060113
#define PLL_1050M    0x1308020d
#define PLL_1000M    0x1302007d
#define PLL_950M     0x130801db
#define PLL_933M     0x130400e9
#define PLL_900M     0x130400e1
#define PLL_867M     0x13060145
#define PLL_850M     0x13070174

#define PLL_800M     0x13010032
#define PLL_750M     0x1402007d
#define PLL_733M     0x130500e5  //732.8
#define PLL_700M     0x140300af
#define PLL_667M     0x14050116
#define PLL_650M     0x14060145
#define PLL_600M     0x1502007d
#define PLL_550M     0x14060113
#define PLL_533M     0x16080215
#define PLL_500M     0x1602007d
#define PLL_467M     0x160801d3

#define PLL_450M     0x1708020d
#define PLL_400M     0x170300af
#define PLL_350M     0x160400af
#define PLL_333M     0x17070154
#define PLL_300M     0x170400af
#define PLL_267M     0x1608010b
#define PLL_250M     0x2602007d
#define PLL_200M     0x270300af
#define PLL_150M     0x3708020d
#define PLL_100M     0x470300af
#define PLL_50M      0x670400af

#endif /* _D2000_PLL_H_ */
