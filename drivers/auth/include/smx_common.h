#ifndef _SMX_H_
#define _SMX_H_

#ifdef __cplusplus
extern "C" {
#endif


#include <stdint.h>    //including int32_t definition
//#include <soc.h>

#define ENABLE_TRNG
#define SRAM_BASE		0x29800000	/* 128KB: SRAM Space */
#define SRAM_SIZE		0x20000		/* 128KB: SRAM Space */

#define SMX_DMA_BASE				(SRAM_BASE + 0x5000UL)   //just for temporary use

#define SMX_BASE_ADDR				(0x28220000UL)                //SMX register base address
#define DMA_BASE_ADDR				(SMX_BASE_ADDR+0x100)         //SMX DMA register base address
#define SKE_BASE_ADDR				(SMX_BASE_ADDR+0x1000)        //SM4 register base address
#define HASH_BASE_ADDR				(SMX_BASE_ADDR+0x2000)        //SM3 register base address
#define TRNG_BASE_ADDR				(SMX_BASE_ADDR+0x3000)        //TRNG register base address
#define PKE_BASE_ADDR				(SMX_BASE_ADDR+0x5000)        //PKE register base address



#define SMX_CR                 (*(volatile unsigned int *)(SMX_BASE_ADDR))
#define SMX_CMD                (*(volatile unsigned int *)(SMX_BASE_ADDR+0x04))
#define SMX_CFG                (*(volatile unsigned int *)(SMX_BASE_ADDR+0x08))
#define SMX_SR1                (*(volatile unsigned int *)(SMX_BASE_ADDR+0x10))
#define SMX_SR2                (*(volatile unsigned int *)(SMX_BASE_ADDR+0x14))
#define SMX_CMD_SR             (*(volatile unsigned int *)(SMX_BASE_ADDR+0x20))
#define SMX_VERSION            (*(volatile unsigned int *)(SMX_BASE_ADDR+0x30))


#define SMX_DMA_CFG            (*(volatile unsigned int *)(DMA_BASE_ADDR))
#define SMX_DMA_SR             (*(volatile unsigned int *)(DMA_BASE_ADDR+0x04))
#define SMX_DMA_TO_THRES       (*(volatile unsigned int *)(DMA_BASE_ADDR+0x08))
#define SMX_DMA_SADDR0         (*(volatile unsigned int *)(DMA_BASE_ADDR+0x10))
#define SMX_DMA_SADDR1         (*(volatile unsigned int *)(DMA_BASE_ADDR+0x14))
#define SMX_DMA_DADDR0         (*(volatile unsigned int *)(DMA_BASE_ADDR+0x20))
#define SMX_DMA_DADDR1         (*(volatile unsigned int *)(DMA_BASE_ADDR+0x24))
#define SMX_DMA_LEN            (*(volatile unsigned int *)(DMA_BASE_ADDR+0x30))


#define SMX_PRINT_BUF
#ifdef SMX_PRINT_BUF
extern void print_buf_U8(uint8_t buf[], uint32_t byteLen, char name[]);
extern void print_buf_U32(uint32_t buf[], uint32_t wordLen, char name[]);
#endif

extern void smx_uint32_copy(uint32_t *dst, uint32_t *src, uint32_t wordLen);

extern void smx_uint32_clear(uint32_t *a, uint32_t wordLen);

extern void smx_reverse_word(uint8_t *in, uint8_t *out, uint32_t bytelen);

extern void smx_dma_reverse_word(uint32_t *in, uint32_t *out, uint32_t wordlen);

extern int32_t uint32_BigNumCmp(uint32_t *a, uint32_t aWordLen, uint32_t *b, uint32_t bWordLen);




#ifdef __cplusplus
}
#endif

#endif

