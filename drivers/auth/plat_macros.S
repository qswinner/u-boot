/*
 * =====================================================================================
 *
 *       Filename:  plat_macros.S
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2017年12月14日 15时51分05秒
 *       Revision:  none
 *
 *         Author:  lixinde (lxd), lixinde@phytium.com.cn
 *        Company:  Phytium Technology Co.,Ltd
 *        License:  BSD-3-Clause
 *
 * =====================================================================================
 */

#ifndef __PLAT_MACROS_S__
#define __PLAT_MACROS_S__


/* ---------------------------------------------
 * The below required platform porting macro
 * prints out relevant platform registers
 * whenever an unhandled exception is taken in
 * BL31.
 * ---------------------------------------------
 */
 
.macro plat_crash_print_regs
/*	mov_imm	x17, GICC_BASE
	mov_imm	x16, GICD_BASE
	arm_print_gic_regs*/
.endm

#endif /* __PLAT_MACROS_S__ */
