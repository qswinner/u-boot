/*
* =====================================================================================
*
*       Filename: e_printf.h
*
*    Description:
*
*        Version:  1.0
*        Created:  2020年10月02日 15时39分
*
*         Author:  lixinde (lxd), lixinde@phytium.com.cn
*        Company:  Phytium Technology Co.,Ltd
*        License:  BSD-3-Clause
*
* =====================================================================================
*/
#ifndef _E_PRINTF_H
#define _E_PRINTF_H

void ealry_serial_putc(const char c);
#endif
