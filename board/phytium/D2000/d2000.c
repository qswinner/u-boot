#include <common.h>
#include <asm/armv8/mmu.h>
#include <asm/system.h>
#include <asm/io.h>
#include <linux/arm-smccc.h>
#include <linux/kernel.h>
#include <scsi.h>
#include "cpu.h"
#include <environment.h>
#include "do_parameter.h"
#include <linux/libfdt.h>
#include <fdt_support.h>
#include <nvme.h>
#include "d2000_common.h"
#include <mmio.h>

DECLARE_GLOBAL_DATA_PTR;

static int scsi_curr_dev; /* current device */

#define OEM_MEM_INFO	0xc2000006

typedef struct mem_info {
	uint64_t res0[2];
	uint64_t capacity0;
	uint64_t res1[5];
	uint64_t capacity1;
	uint64_t res2[5];
}__attribute__((aligned(sizeof(unsigned long)))) mem_info_t;


int dram_init(void)
{
	gd->mem_clk = 0;
	gd->ram_size = PHYS_SDRAM_1_SIZE;	

	uint32_t s3_flag = 0;
	ddr_init(s3_flag);

	return 0;
}

int dram_init_banksize(void)
{

	struct arm_smccc_res res;
	mem_info_t mem_info;
	uint64_t capacity_ddr;

	arm_smccc_smc(0xc2000006, (uint64_t)&mem_info, 0x70, 0, 0, 0, 0, 0, &res);
	
	capacity_ddr = (mem_info.capacity0 + mem_info.capacity1);
	
	uint64_t total_size = (capacity_ddr << 20);

	//printf("total_size=0x%llx\n",total_size);
	
	if(total_size <= PHYS_SDRAM_1_SIZE)
	{
		gd->bd->bi_dram[0].start = PHYS_SDRAM_1;
		gd->bd->bi_dram[0].size =  total_size;
		gd->bd->bi_dram[1].start = PHYS_SDRAM_2;
		gd->bd->bi_dram[1].size =  0x0;
	}
	else
	{
		gd->bd->bi_dram[0].start = PHYS_SDRAM_1;
		gd->bd->bi_dram[0].size =  PHYS_SDRAM_1_SIZE;
		gd->bd->bi_dram[1].start = PHYS_SDRAM_2;
		gd->bd->bi_dram[1].size =  total_size - 0x80000000;
	}

	return 0;
}

int board_init(void)
{
#ifdef CONFIG_FOR_DAO_OS
	/*For dao os wake slave cpu*/
	struct arm_smccc_res res;
	printf("smc wake slave core \n");
	arm_smccc_smc(0xc4000003, 1, 0x88000000, 0, 0, 0, 0, 0, &res);
	udelay(100000);
	arm_smccc_smc(0xc4000003, 0x100, 0x88000000, 0, 0, 0, 0, 0, &res);
    udelay(100000);
    arm_smccc_smc(0xc4000003, 0x101, 0x88000000, 0, 0, 0, 0, 0, &res);
#endif
	return 0;
}

void reset_cpu(ulong addr)
{
	struct arm_smccc_res res;
	printf("run in reset_cpu\n");
	arm_smccc_smc(0x84000009, 0, 0, 0, 0, 0, 0, 0, &res);
	printf("reset cpu error , %lx\n", res.a0);
}

static struct mm_region d2000_mem_map[] = {
	{
		.virt = 0x0UL,
		.phys = 0x0UL,
		.size = 0x80000000UL,
		.attrs = PTE_BLOCK_MEMTYPE(MT_DEVICE_NGNRNE) |PTE_BLOCK_NON_SHARE|PTE_BLOCK_PXN|PTE_BLOCK_UXN
	},
	{
		.virt = (u64)PHYS_SDRAM_1,
		.phys = (u64)PHYS_SDRAM_1,
		.size = (u64)PHYS_SDRAM_1_SIZE,
		.attrs = PTE_BLOCK_MEMTYPE(MT_NORMAL) |PTE_BLOCK_NS|PTE_BLOCK_INNER_SHARE
	},
#ifdef	PHYS_SDRAM_2
	{
		.virt = (u64)PHYS_SDRAM_2,
		.phys = (u64)PHYS_SDRAM_2,
		.size = (u64)PHYS_SDRAM_2_SIZE,
		.attrs = PTE_BLOCK_MEMTYPE(MT_NORMAL) |PTE_BLOCK_NS|PTE_BLOCK_INNER_SHARE
	},
#endif
	{
		0,
	}
};
struct mm_region *mem_map = d2000_mem_map;

void gmac_delay_setup(void)
{
    //printf("gmac delay setup \n");
	//gmac 0
	mmio_write_32(0x28180360, (mmio_read_32(0x28180360) & 0xffff) | 0x21000000);
	//gmac 1
	mmio_write_32(0x2818037C, (mmio_read_32(0x2818037C) & 0xffff) | 0x21000000);
	//set gmac 1 pad
	mmio_write_32(0x28180228, 0x1111100);
}


/*
 * Processor Specific Initialization
 */
int arch_cpu_init(void)
{
	//icache_enable();
	return 0;
}
int mach_cpu_init(void)
{
	check_reset();
	return 0;
}

int board_early_init_f (void)
{
	pcie_init();
	return 0;
}

int board_early_init_r (void)
{
	gmac_delay_setup();
	return 0;
}

/*second_int need use dram and memtest in bl1*/
int testdram(void)
{
	uint32_t s3_flag = 0;
	sec_init(s3_flag);
	printf("ret done \n");

	//register_pfdi();
	return 0;
}

int reserve_arch(void)
{
	register_pfdi();
	return 0;
}

int print_cpuinfo(void)
{
	printf("CPU: Phytium D2000 %ld MHz\n", gd->cpu_clk);
	return 0;
}

int __asm_flush_l3_dcache(void)
{
	int i, pstate;

	for (i = 0; i < HNF_COUNT; i++)
		writeq(HNF_PSTATE_SFONLY, HNF_PSTATE_REQ + i * HNF_STRIDE);
	for (i = 0; i < HNF_COUNT; i++) {
		do {
			pstate = readq(HNF_PSTATE_STAT + i * HNF_STRIDE);
		} while ((pstate & 0xf) != (HNF_PSTATE_SFONLY << 2));
	}

	for (i = 0; i < HNF_COUNT; i++)
		writeq(HNF_PSTATE_FULL, HNF_PSTATE_REQ + i * HNF_STRIDE);

	return 0;
}

static int phytium_scsi_scan(void)
{
	int ret;
	char* argv[] = {"scsi","scan"};

	ret = scsi_scan(true);
	if(ret)
		return ret;
	
	return blk_common_cmd(ARRAY_SIZE(argv), argv, IF_TYPE_SCSI, &scsi_curr_dev);
}
uint32_t					 mCmd_Write;
uint32_t					 mCmd_Eares;
uint32_t					 mCmd_Pp;

int last_stage_init(void)
{
	int ret;
	FLASH_CMD_INFO flash_cmd;	
	/* pci e */
	pci_init();
	/* scsi scan */
	ret = phytium_scsi_scan();
	if(ret != 0)
		printf("scsi scan failed\n");
	/*nvme scan */
	printf("nvme scan start\n");
	ret = nvme_scan_namespace();
	if(ret != 0)
		printf("scsi scan failed\n");
	/*get flash cmd*/
		struct arm_smccc_res res;
	invalidate_dcache_range((unsigned long) &flash_cmd, (unsigned long) (&flash_cmd + sizeof(FLASH_CMD_INFO)));
	printf("get flash cmd\n");
	arm_smccc_smc(0xC200000C, (unsigned long)&flash_cmd, sizeof(FLASH_CMD_INFO), 0, 0, 0, 0, 0, &res);
	mCmd_Write = flash_cmd.Flash_Write;
	mCmd_Eares = flash_cmd.Flash_Erase;
	mCmd_Pp    = flash_cmd.Flash_Pp;
	printf("cmd write is 0x%x,cmd erase is 0x%x,cmd pp is 0x%x\n",flash_cmd.Flash_Write,flash_cmd.Flash_Erase,flash_cmd.Flash_Pp);
	return 0;
}

