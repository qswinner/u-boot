




/**********************EARLY SETUP SMC ID*************************************/
#define CPU_SVC_VERSION     0xC2000F00
#define CPU_GET_RST_SOURCE  0xC2000F01
#define CPU_INIT_PLL        0xC2000F02
#define CPU_INIT_PCIE       0xC2000F03
#define CPU_INIT_MEM        0xC2000F04
#define CPU_INIT_SEC_SVC    0xC2000F05
#define CPU_SECURITY_CFG	0xC2000F07

/******************************************************************/
#ifndef USE_PARAMETER		//当不使用参数表时,在这里设置默认参数

/*********************parameter cpu*******************************************/
#define PARAMETER_CPU_MAGIC					0x54460010

/**********************parameter fw comon************************************/
#define PARAMETER_COMMON_MAGIC					0x54460013


/**********************parameter pcie******************************************/
#define PARAMETER_PCIE_MAGIC				0x54460011

/**********defult PEU set*************/
#define CONFIG_PCI_PEU0 		0x1
#define CONFIG_PCI_PEU1         0x1  /* 0x0 : disable peu1,  x01 : enable peu1 configuartions */
/*0:单根    1:多根*/
#define CONFIG_INDEPENDENT_TREE	0x0

/* splie mode of peu, both PEU0 and PEU1 should be the same */
#define X16                     0x0
#define X8X8                    0x1
#define CONFIG_PEU0_SPLIT_MODE  X16
#define CONFIG_PEU1_SPLIT_MODE  X16

/* peu device mode: 0 ep, 1 rc default */
#define EP_MODE                 0x0
#define RC_MODE                 0x1

#define PEU1_OFFSET	16

#define PEU_C_OFFSET_MODE	16
#define PEU_C_OFFSET_SPEED	0

#define GEN3	3
#define GEN2	2
#define GEN1	1
#endif
/*****************************S3flag get*******************************************/
#define GPIO0_BASE		(MIO_BASE + 0x4000)
#define GPIO1_BASE		(MIO_BASE + 0x5000)

#define SWPORTA_DDR_OFFSET 	 0x4
#define SWPORTB_DDR_OFFSET 	 0x10
#define EXT_PORTA_OFFSET 	 0x8
#define EXT_PORTB_OFFSET 	 0x14

/*GPIO*/
typedef struct pad_info{
	uint32_t pad_sel_reg;
	uint32_t bit_offset;
	uint32_t gpio_swport_ddr;
	uint32_t gpio_ext_port;
	uint32_t port;
}pad_info_t;


void ddr_init(uint8_t s3_flag);
void check_reset(void);
void pcie_init(void);
void sec_init(uint8_t s3_flag);
void register_pfdi(void);
void send_cpld_ctr(uint32_t cmd);

