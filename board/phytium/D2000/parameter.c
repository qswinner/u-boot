#include <common.h>
#include "parameter.h"
#include "mcu/mcu_info.h"
#include <e_uart.h>

void get_pm_pll_info(pll_config_t *pm_data)
{
	pll_config_t const * const pll_ptr = (pll_config_t *)(O_PARAMETER_BASE + PM_PLL_OFFSET);
	memcpy(pm_data, pll_ptr, sizeof(pll_config_t));
	mcu_config_t const * const mcu_ptr = (mcu_config_t *)(O_PARAMETER_BASE + PM_MCU_OFFSET);
	if(mcu_ptr->misc_enable & 0x1){
		//读取spd频率
		p_printf("read spd freq...\n");
		uint32_t spd_freq = i2c_read_spd_lmu_freq(mcu_ptr->ch_enable);
		if(spd_freq)
			pm_data->lmu_pll = pll_ptr->lmu_pll > spd_freq ? spd_freq : pll_ptr->lmu_pll;
	}
	p_printf("PARAMETER_pll --- v%x.%x\n", (pm_data->version>>16)&0xff, pm_data->version&0xff);
	
#if 0
	p_printf("PLL INFO: \n");
	p_printf("\tmagic = 0x%x\n", pm_data->magic);
	p_printf("\tversion = 0x%x\n", pm_data->version);
	p_printf("\tsize = 0x%x\n", pm_data->size);
	p_printf("\tcore_pll = %d\n", pm_data->core_pll);
	p_printf("\tlmu_pll = %d\n", pm_data->lmu_pll);
#endif
}

void get_pm_peu_info(peu_config_t *pm_data)
{
	peu_config_t const * const peu_ptr = (peu_config_t *)(O_PARAMETER_BASE + PM_PCIE_OFFSET);
	memcpy(pm_data, peu_ptr, sizeof(peu_config_t));

	p_printf("PARAMETER_peu --- v%x.%x\n", (pm_data->version>>16)&0xff, pm_data->version&0xff);

}
void get_pm_common_info(common_config_t *pm_data)
{
	common_config_t const * const common_ptr = (common_config_t *)(O_PARAMETER_BASE + PM_COMMON_OFFSET);
	memcpy(pm_data, common_ptr, sizeof(common_config_t));

	p_printf("PARAMETER_common---v%x.%x\n", (pm_data->version>>16)&0xff, pm_data->version&0xff);
}
void get_pm_mcu_info(mcu_config_t *pm_data)
{
	mcu_config_t const * const mcu_ptr = (mcu_config_t *)(O_PARAMETER_BASE + PM_MCU_OFFSET);
	memcpy(pm_data, mcu_ptr, sizeof(mcu_config_t));

	p_printf("PARAMETER_mcu---v%x.%x\n", (pm_data->version>>16)&0xff, pm_data->version&0xff);
}
