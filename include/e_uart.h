/*
* =====================================================================================
*
*       Filename: e_uart.h
*
*    Description:
*
*        Version:  1.0
*        Created:  2020年10月02日 16时22分
*
*         Author:  lixinde (lxd), lixinde@phytium.com.cn
*        Company:  Phytium Technology Co.,Ltd
*        License:  BSD-3-Clause
*
* =====================================================================================
*/
#ifndef _E_UART_H_
#define _E_UART_H_

int p_printf(const char *fmt, ...);
#endif
